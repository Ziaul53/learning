let count =0;
const addOne=() =>{
    count++;
    rendercounterapp();
};
const minusOne=() =>{
    count--;
    rendercounterapp();
};
const reset=() =>{
    count=0;
    rendercounterapp();
};
var appRoot = document.getElementById('app');
const rendercounterapp = () =>{
    const templateTwo = (
        <div>
           <h1>Counter: {count}</h1>
           <button onClick={addOne}>+1</button>
           <button onClick={minusOne}>-1</button>
           <button onClick={reset}>reset</button>
        </div>
    );
    ReactDOM.render(templateTwo,appRoot);
};