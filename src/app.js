console.log('App.js is running!');


// var template = <p>This is JSX from app.js!</p>;    
const app ={
    title: 'Indecicion',
    subtitle: 'This is subtitle',
    options: []
};

const onFormsubmit = (e) =>{
    e.preventDefault();

    const option =e.target.elements.option.value;
    if(option){
        app.options.push(option);
        e.target.elements.option.value='';
        render();
    }
}
const appRoot = document.getElementById('app');
const remove = ()=>{
      app.options=[];
      render();
}
const render = ()=>{
    const template = (
        <div>
        <h1>{app.title}</h1> 
        {app.subtitle && <p>{app.subtitle}</p>}
        <p>{app.options.length >0 ? 'Here are your option' : 'No option'}</p>
        <p>{app.options.length}</p>
        <button onClick={remove}>Remove All</button>
        <ol>
         <li>item 1</li>
         <li>item 2</li>
        </ol>
        <form onSubmit={onFormsubmit}>
         <input type="text" name="option" />
         <button>Add Option</button>
        </form>
        </div>);
    ReactDOM.render(template,appRoot);
};

render();




